jQuery(function($) {
    var nextgen_fancybox_init = function() {
    		var selector = nextgen_lightbox_filter_selector($, $(".ngg-fancybox"));
    		
        selector.fancybox({
            titlePosition: 'inside',
            // Needed for twenty eleven
            onComplete: function() {
                $('#fancybox-wrap').css('z-index', 10000);
				if($("#fancybox-wrap #fancybox-outer .titolo").length > 0)
				{
					$("#fancybox-wrap #fancybox-outer .titolo").html("");
					
				}
				else{
				 $("#fancybox-wrap #fancybox-outer").prepend('<div class="titolo"></div>')
				}
				if($("#fancybox-wrap #fancybox-outer .logo_gal").length > 0)
				{
					$("#fancybox-wrap #fancybox-outer .logo_gal").html("");
					
				}
				else{
				 $("#fancybox-wrap #fancybox-outer").prepend('<div class="logo_gal"></div>')
				}
				$("#fancybox-wrap #fancybox-outer .titolo_alt").prependTo("#fancybox-wrap #fancybox-outer .titolo");
				
				if($("#fancybox-wrap #fancybox-outer .pop_menu").length != 0)
					$("#fancybox-wrap #fancybox-outer .pop_menu").remove();
				
				$("#fancybox-wrap #fancybox-outer").append('<div class="pop_menu"></div>');
				$("#fancybox-wrap #fancybox-outer .pop_menu").html($(".link_box").html());
				 
            }
        });
    };
    $(this).bind('refreshed', nextgen_fancybox_init);
    nextgen_fancybox_init();
});
