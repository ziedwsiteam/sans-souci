<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;
global $shop_style;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';

// Check for second image
$product_gallery = $product->get_gallery_attachment_ids();

$disable_hover_slide = get_field('mpc_disable_hover_slide');
$custom_hover_image = get_field('mpc_custom_hover_image');
$display_second_image = '';
if (!$disable_hover_slide && (! empty($product_gallery) || $custom_hover_image))
	$display_second_image = ' mpcth-double-image';

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('mpcth-waypoint ' . implode($classes, ' ') . $display_second_image); ?> >
	<div class="mpcth-product-wrap">
		<?php woocommerce_show_product_loop_sale_flash(); ?>
		<header class="mpcth-post-header">
			<a class="mpcth-post-thumbnail" href="<?php the_permalink(); ?>">
				<?php do_action('mpcth_before_shop_loop_item_title'); ?>
			</a>
			<?php if (shortcode_exists('yith_wcwl_add_to_wishlist')) echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
		</header>
		<section class="mpcth-post-content">
		<?php if (isset($shop_style)) { ?>
			<?php if ($shop_style == 'slim') { ?>
				<div class="mpcth-post-content-wrap">
					<?php woocommerce_template_loop_price(); ?>
					<h6 class="mpcth-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
					<?php mpcth_wc_product_categories(); ?>
					<div class="mpcth-cart-wrap">
						<?php woocommerce_template_loop_add_to_cart(); ?>
						<?php if (shortcode_exists('yith_wcwl_add_to_wishlist')) echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
					</div>
				</div>
			<?php } elseif ($shop_style == 'center') { ?>
				<div class="mpcth-post-content-wrap">
					<?php woocommerce_template_loop_price(); ?>
					<h6 class="mpcth-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
					<?php mpcth_wc_product_categories(); ?>
					<div class="mpcth-price-wrap">
						<?php woocommerce_template_loop_price(); ?>
					</div>
					<div class="mpcth-cart-wrap">
						<?php woocommerce_template_loop_add_to_cart(); ?>
						<?php if (shortcode_exists('yith_wcwl_add_to_wishlist')) echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
					</div>
				</div>
			<?php } else { ?>
				<div class="mpcth-cart-wrap">
					<?php woocommerce_template_loop_price(); ?>
					<?php woocommerce_template_loop_add_to_cart(); ?>
				</div>
				<h6 class="mpcth-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
				<?php mpcth_wc_product_categories(); ?>
			<?php } ?>
		<?php } else { ?>
			<div class="mpcth-cart-wrap">
				<?php woocommerce_template_loop_price(); ?>
				<?php woocommerce_template_loop_add_to_cart(); ?>
			</div>
			<h6 class="mpcth-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
			<?php mpcth_wc_product_categories(); ?>
		<?php } ?>
		</section>
	</div>
</article>