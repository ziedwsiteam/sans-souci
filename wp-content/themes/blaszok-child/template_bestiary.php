<head>
<link href='../page1.css' rel='stylesheet' type='text/css' />
</head>
<?php

/**

 * Template Name: bestiary

 */
get_header();



global $page_id;

global $paged;



?>
<style>
html {
    margin-top: 0px !important;
}
#mpcth_page_header_wrap {
    background: none repeat scroll 0 0 #fff;
    border-bottom: medium none;
     box-shadow: none;
    position: relative;
    transition: background 0.25s ease-out 0s;
    z-index: 1020;
}
#mpcth_page_header_wrap #mpcth_page_header_container {
    opacity: 1;
    transition: opacity 0.5s ease-out 0s;
    z-index: 2;
	background-color:#fff;
}
#mpcth_page_header_secondary_content {
    border-bottom: medium none;
}
#mpcth_page_wrap {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    min-height: 100%;
}
body { background-color:#fff;} 
.mpcth_content_wrap {
    margin: 244px auto 0;
    width: 300px;
}
.mpcth-sidebar-right #mpcth_content_wrap {
    border-right: medium none;
    color: #000000;
    float: none;
    font-size: 24px;
    line-height: 24px;
    margin: 100px auto 0;
    padding-left: 0 !important;
    padding-right: 0 !important;
     width: 1198px;
}
.ngg-fancybox > img {
    float: left;
    height: 81px;
    margin-bottom: 0;
    margin-left: 0;
    margin-top: 0;
    width: 57px;
	padding: 1px
}
.link_box {
    font-size: 14px;
    font-weight: 500;
    margin: 0 auto;
    width: 671px;
}
.mpcth-post-content-wrap .link_box > a {
    display: inline-block;
    float: left !important;
    margin-bottom: 30px;
    margin-left: 59px;
    margin-top: 130px;
	color:#000000;
}
.link_box > a:hover {
    color: #e80047 !important;
}
.link_box br {
    display: none;
}
.sub-menu {
    display: none;
}
.mpcth-menu .page_item.menu-item-has-children > a:after, .mpcth-menu .menu-item.menu-item-has-children > a:after, #mpcth_menu .page_item.menu-item-has-children > a:after, #mpcth_menu .menu-item.menu-item-has-children > a:after {
    content: none;
}
#mpcth_footer {
    border-top: 1px solid #eeeeee;
    clear: both;
    display: none;
}
.slideshowlink {
display: none;
}
#wpadminbar {
   display: none;
}
.ngg-gallery-thumbnail img {
    border: medium none;
}
.ngg-gallery-thumbnail-box {
    float: left;
    margin-right: 0;
}
.ngg-fancybox > img:hover {
    position: relative;
    transform: scale(3.3);
    z-index: 1000;
	background-color:#ffffff !important;
}
.ngg-galleryoverview {
    overflow: visible;
}

.logo_gal {
    background-image: url("../wp-content/themes/blaszok-child/log_pop.png");
    background-repeat: no-repeat;
    height: 20px;
    margin-left: 20px;
    margin-top: 10px;
    overflow: hidden;
    position: absolute;
    width: 101px;
}
#fancybox-wrap {
    height: 720px !important;
    left: 0 !important;
    margin: 0 auto;
    right: 0 !important;
    top: 0 !important;
    width: 97% !important;
}
.page.page-id-15.page-parent.page-template.page-template-template_bestiary-php.logged-in.admin-bar.mpcth-sidebar-right.customize-support #fancybox-overlay {
    height: 763px !important;
}
#fancybox-outer #fancybox-content {
    margin: 3% auto 0;
}

.fancybox-title-inside {
display: block;
    height: 100px !important;
    margin: 0 auto !important;
    width: 18% !important;
}
.pop_menu {
    bottom: 0 !important;
    height: 54px;
    margin-bottom: 20px;
    margin-left: 458px;
    position: absolute;
    width: 42%;
}
.pop_menu a {
    color: #000000;
    display: inline-block;
    float: left !important;
    margin-bottom: 0;
    margin-left: 59px;
    margin-top: 36px;
    text-decoration: none;
}
.pop_menu br {
display:none;
}
.pop_menu a:hover {
    color: #e80047 !important;
}
#fancybox-close {
    background-image: url("../wp-content/themes/blaszok-child/exit.png") !important;
    background-position: center center;
    height: 30px !important;
    margin-right: 25px;
    margin-top: 25px;
    width: 27px !important;
}
#fancybox-right-ico {
    background-image: url("../wp-content/themes/blaszok-child/next.png") !important;   
}
#fancybox-left-ico {
    background-image: url("../wp-content/themes/blaszok-child/prev.png") !important;   
}
</style>



		

		<div id="mpcth_content_wrap" class="mpcth_content_wrap">

			<div id="mpcth_content">

				<?php if (have_posts()) : ?>

					<?php while (have_posts()) : the_post();

						$post_meta = get_post_custom($post->ID);

						$post_format = get_post_format();



						if($post_format === false)

							$post_format = 'standard';



						$title = get_the_title();

						$link = get_field('mpc_link_url');

						if($post_format == 'link' && isset($link))

							$title = '<a href="' . $link . '" class="mpcth-color-main-color-hover" title="' . get_the_title() . '">' . get_the_title() . '<i class="fa fa-external-link"></i></a>';

					?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('mpcth-post'); ?> >

				

							<section class="mpcth-post-content">

								<div class="mpcth-post-content-wrap">

									<?php the_content(); ?>

								</div>

								<?php wp_link_pages(); ?>
								
								
								<?php

									$tags = get_the_tag_list('', __(', ', 'mpcth'));

									if ($tags) {

										echo '<div class="mpcth-post-tags">';

											echo __('Tagged as ', 'mpcth') . $tags;

										echo '</div>';

									}

								?>

							</section>

							<footer class="mpcth-post-footer">

								<?php if (comments_open()) { ?>

									<div id="mpcth_comments">

										<?php comments_template('', true); ?>

									</div>

								<?php } ?>

							</footer>

						</article>

					<?php endwhile; ?>

				<?php else : ?>

					<article id="post-0" class="mpcth-post mpcth-post-not-found">

						<header class="mpcth-post-header">

							<div class="mpcth-post-thumbnail">



							</div>

							<h3 class="mpcth-post-title">

								<?php _e('Nothing Found', 'mpcth'); ?>

							</h3>

							<div class="mpcth-post-meta">



							</div>

						</header>

						<section class="mpcth-post-content">

							<?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'mpcth'); ?>

						</section>

						<footer class="mpcth-post-footer">
								
							

						</footer>

					</article>

				<?php endif; ?>

			</div><!-- end #mpcth_content -->
		</div><!-- end #mpcth_content_wrap -->
		
<!--menu-style*/-->
<style>
.mpcth-menu .menu-item-33 {
    margin-left: 13px !important;
}
.mpcth-menu .page_item > a, .mpcth-menu .menu-item > a, #mpcth_menu .page_item > a, #mpcth_menu .menu-item > a {
    color: #000000;
    font-weight: bold;
    margin-left: 35px;
    position: relative;
}
 
</style>
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>	
<script>
$(document).ready(function(){
$(".mpcth-menu .menu-item-40,.mpcth-menu .menu-item-43,.mpcth-menu .menu-item-46").remove();

});
</script>
<script type="text/javascript">
var matched, browser;

jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;
</script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/ajax.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/persist.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/store.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/ngg_store.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/lightbox/static/lightbox_context.js?ver=4.0'></script>



<style>
.titolo {
    color: #000000;
    font-size: 13px;
    font-weight: 500;
    height: 30px;
    line-height: 33px;
   padding: 13px 5px 5px;
    text-align: center;
}
</style>
<style>
@media screen and (-webkit-min-device-pixel-ratio:0) {
   .titolo {
    color: #000000;
    font-size: 13px;
    font-weight: 500;
    height: 30px;
    line-height: 33px;
   padding: 13px 5px 5px;
    text-align: center;
}
html {
    margin-top: 0px !important;
}
#mpcth_page_header_wrap {
    background: none repeat scroll 0 0 #fff;
    border-bottom: medium none;
     box-shadow: none;
    position: relative;
    transition: background 0.25s ease-out 0s;
    z-index: 1020;
}
#mpcth_page_header_wrap #mpcth_page_header_container {
    opacity: 1;
    transition: opacity 0.5s ease-out 0s;
    z-index: 2;
	background-color:#fff;
}
#mpcth_page_header_secondary_content {
    border-bottom: medium none;
}
#mpcth_page_wrap {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    min-height: 100%;
}
body { background-color:#fff;} 
.mpcth_content_wrap {
    margin: 244px auto 0;
    width: 300px;
}
.mpcth-sidebar-right #mpcth_content_wrap {
    border-right: medium none;
    color: #000000;
    float: none;
    font-size: 24px;
    line-height: 24px;
    margin: 100px auto 0;
    padding-left: 0 !important;
    padding-right: 0 !important;
     width: 1198px;
}
.ngg-fancybox > img {
    float: left;
    height: 81px;
    margin-bottom: 0;
    margin-left: 0;
    margin-top: 0;
    width: 57px;
	padding: 1px
}
.link_box {
    font-size: 14px;
    font-weight: 500;
    margin: 0 auto;
    width: 671px;
}
.mpcth-post-content-wrap .link_box > a {
    display: inline-block;
    float: left !important;
    margin-bottom: 30px;
    margin-left: 59px;
    margin-top: 130px;
	color:#000000;
}
.link_box > a:hover {
    color: #e80047 !important;
}
.link_box br {
    display: none;
}
.sub-menu {
    display: none;
}
.mpcth-menu .page_item.menu-item-has-children > a:after, .mpcth-menu .menu-item.menu-item-has-children > a:after, #mpcth_menu .page_item.menu-item-has-children > a:after, #mpcth_menu .menu-item.menu-item-has-children > a:after {
    content: none;
}
#mpcth_footer {
    border-top: 1px solid #eeeeee;
    clear: both;
    display: none;
}
.slideshowlink {
display: none;
}
#wpadminbar {
   display: none;
}
.ngg-gallery-thumbnail img {
    border: medium none;
}
.ngg-gallery-thumbnail-box {
    float: left;
    margin-right: 0;
}
.ngg-fancybox > img:hover {
    position: relative;
    transform: scale(3.3);
    z-index: 1000;
	background-color:#ffffff !important;
}
.ngg-galleryoverview {
    overflow: visible;
}

.logo_gal {
    background-image: url("../wp-content/themes/blaszok-child/log_pop.png");
    background-repeat: no-repeat;
    height: 20px;
    margin-left: 20px;
    margin-top: 10px;
    overflow: hidden;
    position: absolute;
    width: 101px;
}
.page.page-id-15.page-parent.page-template.page-template-template_bestiary-php.logged-in.admin-bar.mpcth-sidebar-right.customize-support #fancybox-wrap {
    height: 720px !important;
    left: 0 !important;
    margin: 0 auto;
    right: 0;
    top: 0 !important;
    width: 97% !important;
}
.page.page-id-15.page-parent.page-template.page-template-template_bestiary-php.logged-in.admin-bar.mpcth-sidebar-right.customize-support #fancybox-overlay {
    height: 763px !important;
}
#fancybox-outer #fancybox-content {
    margin: 3% auto 0;
}

.fancybox-title-inside {
display: block;
    height: 100px !important;
    margin: 0 auto !important;
    width: 18% !important;
}
.pop_menu {
    bottom: 0 !important;
    height: 54px;
    margin-bottom: 20px;
    margin-left: 458px;
    position: absolute;
    width: 42%;
}
.pop_menu a {
    color: #000000;
    display: inline-block;
    float: left !important;
    margin-bottom: 0;
    margin-left: 59px;
    margin-top: 36px;
    text-decoration: none;
}
.pop_menu br {
display:none;
}
.pop_menu a:hover {
    color: #e80047 !important;
}
#fancybox-close {
    background-image: url("../wp-content/themes/blaszok-child/exit.png") !important;
    background-position: center center;
    height: 30px !important;
    margin-right: 25px;
    margin-top: 25px;
    width: 27px !important;
}
#fancybox-right-ico {
    background-image: url("../wp-content/themes/blaszok-child/next.png") !important;   
}
#fancybox-left-ico {
    background-image: url("../wp-content/themes/blaszok-child/prev.png") !important;   
}
}
</style>



<?php get_footer();


