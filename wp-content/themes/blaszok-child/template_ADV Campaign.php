<head>
<link href='../wp-content/themes/blaszok-child/page1.css' rel='stylesheet' type='text/css' />
</head>
<?php

/**

 * Template Name: ADV Campaign

 */
get_header();



global $page_id;

global $paged;



?>


<style>
html {
    margin-top: 0px !important;
}
#mpcth_page_header_wrap {
    background: none repeat scroll 0 0 #fff;
    border-bottom: medium none;
     box-shadow: none;
    position: relative;
    transition: background 0.25s ease-out 0s;
    z-index: 1020;
}
#mpcth_page_header_wrap #mpcth_page_header_container {
    opacity: 1;
    transition: opacity 0.5s ease-out 0s;
    z-index: 2;
	background-color:#fff;
}
#mpcth_page_header_secondary_content {
    border-bottom: medium none;
}
#mpcth_page_wrap {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    min-height: 100%;
}
body { background-color:#fff;} 
.mpcth_content_wrap {
    margin: 244px auto 0;
    width: 300px;
}
.mpcth-sidebar-right #mpcth_content_wrap {
    border-right: medium none;
    color: #000000;
    float: none;
    font-size: 24px;
    line-height: 24px;
    margin: 15px auto 0;
    padding-left: 0 !important;
    padding-right: 0 !important;
    width: 100%;
}
.mpcth-post-content-wrap img {
    border: 0 none;
    height: auto;
    width: 245px;
}
#map-canvas {
    height: 550px;
    margin: -28px auto 0;
    width: 850px;
}

.link_box1 {
    font-size: 14px;
    font-weight: 500;
    margin: 0 auto;
    width: 671px;
}
.mpcth-post-content-wrap .link_box1 > a {
    color: #000000;
    display: inline-block;
    float: left !important;
    margin-bottom: 20px;
    margin-left: 59px;
    margin-top: -20px;
}
.link_box1 > a:hover {
    color: #e80047 !important;
}
.link_box1 br {
    display: none;
}
.sub-menu {
    display: none;
}
.mpcth-menu .page_item.menu-item-has-children > a:after, .mpcth-menu .menu-item.menu-item-has-children > a:after, #mpcth_menu .page_item.menu-item-has-children > a:after, #mpcth_menu .menu-item.menu-item-has-children > a:after {
    content: none;
}
.link_box .prev-page {
    background-image: url("../wp-content/themes/blaszok-child/prev.png") !important;
    background-position: 9px 2px;
    background-repeat: no-repeat;
    height: 28px;
    left: 0;
    margin-left:10px !important;
    margin-top: -311px !important;
    padding: 0 !important;
    position: absolute;
    width: 36px;
}
.link_box .prev-page.disabled {
background-image: none !important;
display:none !important;
}
.link_box .next-page.disabled {
background-image: none !important;
display:none !important;
}
.link_box .next-page {
    background-image: url("../wp-content/themes/blaszok-child/next.png") !important;
    background-position: -9px 2px;
    background-repeat: no-repeat;
    height: 28px;
    right: 0;
    margin-left: 0 !important;
	margin-right:10px !important;
    margin-top: -311px !important;
    padding: 0 !important;
    position: absolute;
    width: 36px;
}
</style>



		

		<div id="mpcth_content_wrap" class="mpcth_content_wrap">

			<div id="mpcth_content">

				<?php if (have_posts()) : ?>

					<?php while (have_posts()) : the_post();

						$post_meta = get_post_custom($post->ID);

						$post_format = get_post_format();



						if($post_format === false)

							$post_format = 'standard';



						$title = get_the_title();

						$link = get_field('mpc_link_url');

						if($post_format == 'link' && isset($link))

							$title = '<a href="' . $link . '" class="mpcth-color-main-color-hover" title="' . get_the_title() . '">' . get_the_title() . '<i class="fa fa-external-link"></i></a>';

					?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('mpcth-post'); ?> >

				

							<section class="mpcth-post-content">

								<div class="mpcth-post-content-wrap">

									<?php the_content(); ?>

								</div>

								<?php wp_link_pages(); ?>
								
								
								<?php

									$tags = get_the_tag_list('', __(', ', 'mpcth'));

									if ($tags) {

										echo '<div class="mpcth-post-tags">';

											echo __('Tagged as ', 'mpcth') . $tags;

										echo '</div>';

									}

								?>

							</section>

							<footer class="mpcth-post-footer">

								<?php if (comments_open()) { ?>

									<div id="mpcth_comments">

										<?php comments_template('', true); ?>

									</div>

								<?php } ?>

							</footer>

						</article>

					<?php endwhile; ?>

				<?php else : ?>

					<article id="post-0" class="mpcth-post mpcth-post-not-found">

						<header class="mpcth-post-header">

							<div class="mpcth-post-thumbnail">



							</div>

							<h3 class="mpcth-post-title">

								<?php _e('Nothing Found', 'mpcth'); ?>

							</h3>

							<div class="mpcth-post-meta">



							</div>

						</header>

						<section class="mpcth-post-content">

							<?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'mpcth'); ?>

						</section>

						<footer class="mpcth-post-footer">
								
							

						</footer>

					</article>

				<?php endif; ?>

			</div><!-- end #mpcth_content -->
		</div><!-- end #mpcth_content_wrap -->
<!--menu-style*/-->
<style>
.mpcth-menu .menu-item-33 {
    margin-left: 13px !important;
}
.mpcth-menu .page_item > a, .mpcth-menu .menu-item > a, #mpcth_menu .page_item > a, #mpcth_menu .menu-item > a {
    color: #000000;
    font-weight: bold;
    margin-left: 35px;
    position: relative;
}
 
</style>
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>	
<script>
$(document).ready(function(){
$(".mpcth-menu .menu-item-40,.mpcth-menu .menu-item-43,.mpcth-menu .menu-item-46").remove();
$(".mpcth-post-content-wrap a img").click(function(zied){zied.preventDefault();});
});
</script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/themes/blaszok-child/modernizr.min.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/themes/blaszok-child/highlight.min.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/themes/blaszok-child/slick.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/themes/blaszok-child/scripts.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/themes/blaszok-child/alfie.f51946af45e0b561c60f768335c9eb79.js?ver=4.0'></script>
<script>
$('.fade').slick({
  dots: true,
  infinite: true,
  speed: 500,
  fade: true,
  slide: 'div',
  cssEase: 'linear'
});
</script>
<script>
$(document).ready(function(){
var logo = "<?php bloginfo('url');?>"+"/wp-content/themes/blaszok-child/log_pop.png";
$("#mpcth_logo_wrap #mpcth_logo img").each(function(){
$(this).attr("src",logo);
})
});
</script>
<style>
.mpcth-menu {
    margin-left: 80px !important;
}
#mpcth_page_header_content #mpcth_logo_wrap {
    display: table-cell;
    padding: 9px 1em 1.2em 0;
    vertical-align: middle;
    width: 1em;
}
</style>



