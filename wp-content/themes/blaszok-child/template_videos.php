<head>
<link href='page1.css' rel='../stylesheet' type='text/css' />
</head>
<?php

/**

 * Template Name: videos

 */
get_header();



global $page_id;

global $paged;



?>


<style>
html {
    margin-top: 0px !important;
}
#mpcth_page_header_wrap {
    background: none repeat scroll 0 0 #343434;
    border-bottom: medium none;
     box-shadow: none;
    position: relative;
    transition: background 0.25s ease-out 0s;
    z-index: 1020;
}
#mpcth_page_header_wrap #mpcth_page_header_container {
    opacity: 1;
    transition: opacity 0.5s ease-out 0s;
    z-index: 2;
	background-color:#343434;
}
#mpcth_page_header_secondary_content {
    border-bottom: medium none;
}
#mpcth_page_wrap {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    min-height: 100%;
}
body { background-color:#343434;} 
.mpcth_content_wrap {
    margin: 244px auto 0;
    width: 300px;
}
.mpcth-sidebar-right #mpcth_content_wrap {
    border-right: medium none;
    color: #000000;
    float: none;
    font-size: 24px;
    line-height: 24px;
    margin: 217px auto 0;
    padding-left: 0 !important;
    padding-right: 0 !important;
    width: 1195px;
}
.mpcth-post-content-wrap img {
    border: 0 none;
    height: auto;
    width: 245px;
}
#map-canvas {
    height: 550px;
    margin: -28px auto 0;
    width: 850px;
}

.link_box {
    font-size: 14px;
    font-weight: 500;
    margin: 0 auto;
    width: 671px;
}
.mpcth-post-content-wrap .link_box > a {
    display: inline-block;
    float: left !important;
    margin-bottom: 30px;
    margin-left: 59px;
    margin-top: 130px;
	color:#fff;
}
.link_box > a:hover {
    color: #e80047 !important;
}
.link_box br {
    display: none;
}
.sub-menu {
    display: none;
}
.mpcth-menu .page_item.menu-item-has-children > a:after, .mpcth-menu .menu-item.menu-item-has-children > a:after, #mpcth_menu .page_item.menu-item-has-children > a:after, #mpcth_menu .menu-item.menu-item-has-children > a:after {
    content: none;
}
</style>



		

		<div id="mpcth_content_wrap" class="mpcth_content_wrap">

			<div id="mpcth_content">

				<?php if (have_posts()) : ?>

					<?php while (have_posts()) : the_post();

						$post_meta = get_post_custom($post->ID);

						$post_format = get_post_format();



						if($post_format === false)

							$post_format = 'standard';



						$title = get_the_title();

						$link = get_field('mpc_link_url');

						if($post_format == 'link' && isset($link))

							$title = '<a href="' . $link . '" class="mpcth-color-main-color-hover" title="' . get_the_title() . '">' . get_the_title() . '<i class="fa fa-external-link"></i></a>';

					?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('mpcth-post'); ?> >

				

							<section class="mpcth-post-content">

								<div class="mpcth-post-content-wrap">

									<?php the_content(); ?>

								</div>

								<?php wp_link_pages(); ?>
								
								
								<?php

									$tags = get_the_tag_list('', __(', ', 'mpcth'));

									if ($tags) {

										echo '<div class="mpcth-post-tags">';

											echo __('Tagged as ', 'mpcth') . $tags;

										echo '</div>';

									}

								?>

							</section>

							<footer class="mpcth-post-footer">

								<?php if (comments_open()) { ?>

									<div id="mpcth_comments">

										<?php comments_template('', true); ?>

									</div>

								<?php } ?>

							</footer>

						</article>

					<?php endwhile; ?>

				<?php else : ?>

					<article id="post-0" class="mpcth-post mpcth-post-not-found">

						<header class="mpcth-post-header">

							<div class="mpcth-post-thumbnail">



							</div>

							<h3 class="mpcth-post-title">

								<?php _e('Nothing Found', 'mpcth'); ?>

							</h3>

							<div class="mpcth-post-meta">



							</div>

						</header>

						<section class="mpcth-post-content">

							<?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'mpcth'); ?>

						</section>

						<footer class="mpcth-post-footer">
								
							

						</footer>

					</article>

				<?php endif; ?>

			</div><!-- end #mpcth_content -->
		</div><!-- end #mpcth_content_wrap -->
<!--menu-style*/-->
<style>
.mpcth-menu .menu-item-33 {
    margin-left: 13px !important;
}
.mpcth-menu .page_item > a, .mpcth-menu .menu-item > a, #mpcth_menu .page_item > a, #mpcth_menu .menu-item > a {
    color: #000000;
    font-weight: bold;
    margin-left: 35px;
    position: relative;
}
 .hugeitmicro .hugeitmicro-item:nth-child(1){transform:translate(0,0)!important; width:390px !important;}
 .hugeitmicro .hugeitmicro-item:nth-child(2){transform: translate(404px, 0px) !important; width:390px !important;}
 .hugeitmicro .hugeitmicro-item:nth-child(3){transform: translate(806px, 0px) !important; width:390px !important;}
 .element_1 .image-block_1 {
    position: relative;
    width: 390px !important;
}
.super-list.variable-sizes.clearfix.hugeitmicro {
    height: 220px !important;
}
</style>
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>	
<script>
$(document).ready(function(){
$(".mpcth-menu .menu-item-40,.mpcth-menu .menu-item-43,.mpcth-menu .menu-item-46").remove();
$(".mpcth-post-content-wrap a img").click(function(zied){zied.preventDefault();});
});
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var photocrati_ajax = {"url":"http:\/\/82.85.184.242\/wordpress\/photocrati_ajax","wp_home_url":"http:\/\/82.85.184.242\/wordpress","wp_site_url":"http:\/\/82.85.184.242\/wordpress","wp_root_url":"http:\/\/82.85.184.242\/wordpress","wp_plugins_url":"http:\/\/82.85.184.242\/wordpress\/wp-content\/plugins","wp_content_url":"http:\/\/82.85.184.242\/wordpress\/wp-content","wp_includes_url":"http:\/\/82.85.184.242\/wordpress\/wp-includes\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/ajax.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/persist.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/store.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/ajax/static/ngg_store.js?ver=4.0'></script>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/gallery-video/js/video_gallery-all.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/nextgen-gallery/products/photocrati_nextgen/modules/lightbox/static/lightbox_context.js?ver=4.0'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/photo-gallery/js/bwg_frontend.js?ver=1.2.6'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/photo-gallery/js/jquery.mobile.js?ver=1.2.6'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/photo-gallery/js/jquery.mCustomScrollbar.concat.min.js?ver=1.2.6'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/photo-gallery/js/jquery.fullscreen-0.4.1.js?ver=0.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var bwg_objectL10n = {"bwg_field_required":"champ est obligatoire. ","bwg_mail_validation":"Ce n'est pas une adresse email valide. ","bwg_search_result":"Il n'y a pas d'images correspondant \u00e0 votre recherche."};
/* ]]> */
</script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/photo-gallery/js/bwg_gallery_box.js?ver=1.2.6'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/wonderplugin-carousel/engine/wonderplugincarouselskins.js?ver=3.3'></script>
<script type='text/javascript' src='http://82.85.184.242/wordpress/wp-content/plugins/wonderplugin-carousel/engine/wonderplugincarousel.js?ver=3.3'></script>
<script>
	var lightbox_transition = 'elastic';
	var lightbox_speed = 800;
	var lightbox_fadeOut = 300;
	var lightbox_title = false;
	var lightbox_scalePhotos = true;
	var lightbox_scrolling = false;
	var lightbox_opacity = 0.201;
	var lightbox_open = false;
	var lightbox_returnFocus = true;
	var lightbox_trapFocus = true;
	var lightbox_fastIframe = true;
	var lightbox_preloading = true;
	var lightbox_overlayClose = true;
	var lightbox_escKey = false;
	var lightbox_arrowKey = false;
	var lightbox_loop = true;
	var lightbox_closeButton = false;
	var lightbox_previous = "previous";
	var lightbox_next = "next";
	var lightbox_close = "close";
	var lightbox_html = false;
	var lightbox_photo = false;
	var lightbox_width = "800";
	var lightbox_height = "600";
	var lightbox_innerWidth = 'false';
	var lightbox_innerHeight = 'false';
	var lightbox_initialWidth = '300';
	var lightbox_initialHeight = '100';
	
        var lightbox_maxWidth = '';
        var lightbox_maxHeight = '';
        
	var lightbox_slideshow = false;
	var lightbox_slideshowSpeed = 2500;
	var lightbox_slideshowAuto = true;
	var lightbox_slideshowStart = "start slideshow";
	var lightbox_slideshowStop = "stop slideshow";
	var lightbox_fixed = true;
			var lightbox_top = false;
		var lightbox_bottom = false;
		var lightbox_left = false;
		var lightbox_right = false;
		
	var lightbox_reposition = false;
	var lightbox_retinaImage = true;
	var lightbox_retinaUrl = false;
	var lightbox_retinaSuffix = "@2x.$1";
	
				jQuery(document).ready(function(){
				jQuery("#huge_it_videogallery_content a[href$='.jpg'], #huge_it_videogallery_content a[href$='.png'], #huge_it_videogallery_content a[href$='.gif']").addClass('group1');
				
				
				jQuery(".group1").colorbox({rel:'group1'});
				jQuery(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				jQuery(".vimeo").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				jQuery(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				jQuery(".inline").colorbox({inline:true, width:"50%"});
				jQuery(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				jQuery('.non-retina').colorbox({rel:'group5', transition:'none'})
				jQuery('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				

				jQuery("#click").click(function(){ 
					jQuery('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		
</script>
	<!--Huge IT videogallery START-->
	<!-- videogallery CONTENT POPUP -->
		<link href="http://82.85.184.242/wordpress/wp-content/plugins/gallery-video/Front_end/../style/colorbox-1.css" rel="stylesheet" type="text/css" />
		
	<script src="http://82.85.184.242/wordpress/wp-content/plugins/gallery-video/Front_end/../js/jquery.colorbox.js"></script>
	<script src="http://82.85.184.242/wordpress/wp-content/plugins/gallery-video/Front_end/../js/jquery.hugeitmicro.min.js"></script>
	
		

	
<style type="text/css"> 

.element_1 {
	width:275px;
	margin:0px 0px 10px 0px;
	border:0px solid #eeeeee;
	border-radius:3px;
	outline:none;
	overflow:hidden;
}

.element_1 .image-block_1 {
	position:relative;
	width:275px;
}

.element_1 .image-block_1 a {display:block;}

.element_1 .image-block_1 img {
	width:275px !important;
	height:auto;
	display:block;
	border-radius: 0px !important;
	box-shadow: 0 0px 0px rgba(0, 0, 0, 0) !important; 
}

.element_1 .image-block_1 img:hover {
	cursor: -webkit-zoom-in; cursor: -moz-zoom-in;
}

.element_1 .image-block_1 .play-icon {
	position:absolute;
	top:0px;
	left:0px;
	width:100%;
	height:100%;	
	
}

.element_1 .image-block_1  .play-icon.youtube-icon {background:url(http://82.85.184.242/wordpress/wp-content/plugins/gallery-video/Front_end/../images/play.youtube.png) center center no-repeat;}

.element_1 .image-block_1  .play-icon.vimeo-icon {background:url(http://82.85.184.242/wordpress/wp-content/plugins/gallery-video/Front_end/../images/play.vimeo.png) center center no-repeat;}


.element_1 .title-block_1 {
	position:absolute;
	
	left:0px;
	width:100%;
	padding-top:5px;
	height:30px;
	bottom:-35px;
	background: rgba(0,0,0,0.8)  !important;
	 -webkit-transition: bottom 0.3s ease-out 0.1s;
     -moz-transition: bottom 0.3s ease-out 0.1s;
     -o-transition: bottom 0.3s ease-out 0.1s;
     transition: bottom 0.3s ease-out 0.1s;
}

.element_1:hover .title-block_1 {bottom:0px;}

.element_1 .title-block_1 a, .element_1 .title-block_1 a:link, .element_1 .title-block_1 a:visited {
	position:relative;
	margin:0px;
	padding:0px 1% 0px 2%;
	width:97%;
	text-decoration:none;
	text-overflow: ellipsis;
	overflow: hidden; 
	white-space:nowrap;
	z-index:20;
	font-size: 16px;
	color:#c02121;
	font-weight:normal;
}



.element_1 .title-block_1 a:hover, .element_1 .title-block_1 a:focus, .element_1 .title-block_1 a:active {
	color:#FF2C2C;
	text-decoration:none;
}

</style>
<script> 
 jQuery(function(){
	var defaultBlockWidth=275+20+550;
    var $container = jQuery('#huge_it_videogallery_container_1');
    
    
      // add randomish size classes
      $container.find('.element_1').each(function(){
        var $this = jQuery(this),
            number = parseInt( $this.find('.number').text(), 10 );
			//alert(number);
        if ( number % 7 % 2 === 1 ) {
          $this.addClass('width2');
        }
        if ( number % 3 === 0 ) {
          $this.addClass('height2');
        }
      });
    
$container.hugeitmicro({
  itemSelector : '.element_1',
  masonry : {
	columnWidth : 275+10+0  },
  masonryHorizontal : {
	rowHeight: 'auto'
  },
  cellsByRow : {
	columnWidth : 275,
	rowHeight : 'auto'
  },
  cellsByColumn : {
	columnWidth : 275,
	rowHeight : 'auto'
  },
  getSortData : {
	symbol : function( $elem ) {
	  return $elem.attr('data-symbol');
	},
	category : function( $elem ) {
	  return $elem.attr('data-category');
	},
	number : function( $elem ) {
	  return parseInt( $elem.find('.number').text(), 10 );
	},
	weight : function( $elem ) {
	  return parseFloat( $elem.find('.weight').text().replace( /[\(\)]/g, '') );
	},
	name : function ( $elem ) {
	  return $elem.find('.name').text();
	}
  }
});
    
    
      var $optionSets = jQuery('#huge_it_videogallery_options .option-set'),
          $optionLinks = $optionSets.find('a');

      $optionLinks.click(function(){
        var $this = jQuery(this);

        if ( $this.hasClass('selected') ) {
          return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
  

        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');

        value = value === 'false' ? false : value;
        options[ key ] = value;
        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {

          changeLayoutMode( $this, options )
        } else {

          $container.hugeitmicro( options );
        }
        
        return false;
      });


    

      var isHorizontal = false;
      function changeLayoutMode( $link, options ) {
        var wasHorizontal = isHorizontal;
        isHorizontal = $link.hasClass('horizontal');

        if ( wasHorizontal !== isHorizontal ) {

          var style = isHorizontal ? 
            { height: '100%', width: $container.width() } : 
            { width: 'auto' };

          $container.filter(':animated').stop();

          $container.addClass('no-transition').css( style );
          setTimeout(function(){
            $container.removeClass('no-transition').hugeitmicro( options );
          }, 100 )
        } else {
          $container.hugeitmicro( options );
        }
      }
     
    var $sortBy = jQuery('#sort-by');
    jQuery('#shuffle a').click(function(){
      $container.hugeitmicro('shuffle');
      $sortBy.find('.selected').removeClass('selected');
      $sortBy.find('[data-option-value="random"]').addClass('selected');
      return false;
    });

	  jQuery(window).load(function(){
		$container.hugeitmicro('reLayout');
	  });
  });
  

</script>
<!--<script>
$(document).ready(function(){
$(".element_1.hugeitmicro-item:nth-child(1)").css({
		"border":"20px solid red",
		"transform": "scale(2)",
		"width":"390px"
		
});
$(".element_1.hugeitmicro-item:nth-child(2)").css({
		"border":"20px solid red",
		"transform": "scale(2)",
		"width":"390px"
		
});
$(".element_1.hugeitmicro-item:nth-child(3)").css({
		"border":"20px solid red",
		"transform": "scale(2)",
		"width":"390px"
		
});

});
</script>-->
<style>
.element_1 .image-block_1 img {
    border-radius: 0 !important;
    box-shadow: 0 0 0 rgba(0, 0, 0, 0) !important;
    display: block;
    height: 220px;
    width: 390px !important;
}
</style>
<script>
$(document).ready(function(){
var logo = "<?php bloginfo('url');?>"+"/wp-content/themes/blaszok-child/logo_nn.png";
$("#mpcth_logo_wrap #mpcth_logo img").each(function(){
$(this).attr("src",logo);
})
});
</script>

   


