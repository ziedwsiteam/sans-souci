<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'sanssouci');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'mysql');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_NCx)02 {nlWM6n p1${i$yx8ul2OHEcqmd}:z;XH``?nou%YQZn[ArM[$$+z0*N');
define('SECURE_AUTH_KEY',  'Ib_:>Sbo@ENvSDeaBKQBSgHvZ1i1l6<D>u KFjtuLJJVZp+5&Dac!PO%t?Nl)FE4');
define('LOGGED_IN_KEY',    'F5@kw-q.d4`QC:8_g<97>!vL`xrdFg@Zps,1`:7Z%jF{U&uB!rO. s%B|1mtG~7C');
define('NONCE_KEY',        'b^;L+R{_SS+K]z3V#d~0@8Dy2``?$2pnL36h{<mb4n/_y7|}sFUIBo/Z(o(T}~iq');
define('AUTH_SALT',        '({Qk{D5Rves99#ZLlR%^pk]Q_b#)]|w0:f~!Is]ZA-r{kCDy=T)O6fol0W$tJz}n');
define('SECURE_AUTH_SALT', 'u|b/:`a<6J]iHN;hb^N*L/9U?i/DztX}:;roQk`D4L}cAT>00*>~tC?j8K)>y^>M');
define('LOGGED_IN_SALT',   ')+;htj0B~#Ye~jgGK@~MaR<) tc|d`cS4w-n^WZoe;mE+$AHR7^K@_Jp|+P)o9[e');
define('NONCE_SALT',       '@hZrRAhjVk#K@pWM,dkbDhmY/5?0D1^J1%u71%D^k1ZGZEzMipu4[ebe!n#;j0-W');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
